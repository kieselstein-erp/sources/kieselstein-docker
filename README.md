# kieselstein-docker

This repo contains docker files used within the kieselstein build process.

The main aim here is to prebuild them using the gitlab pipeline, so that they only need to rebuild as soon as something
changes within them.